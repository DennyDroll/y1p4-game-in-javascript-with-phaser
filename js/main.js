//disable keyboard scrolling
window.addEventListener("keydown", function(e) {
  // space and arrow keys
  if([32, 37, 38, 39, 40].indexOf(e.keyCode) > -1) {
      e.preventDefault();
  }
}, false);
window.onload = function() {
  var context = new AudioContext();
 }



var game = new Phaser.Game(1600, 1600, Phaser.AUTO, '', 
{ 
preload: preload,
create: create, 
update: update
});

// #region variables
var player;
var lives = 3;  
var slime1;
var slime2;
var slimeSpeed = 75;
var floor;
var walls;
var obsts;
var above;
var map;
var isAttacking = false;
var timer;
var useTimer;
var total = 0;
var top_torch;
var bot_torch;
var enemies;
var enemies2;
var doors;
var door1;
var door2;
var door3;
var door4;
var door5;
var button1;
var button1Activated = false;
var button2;
var button2Activated = false;
var button3;
var button3Activated = false;
var button4;
var button4Activated = false;
var music;
var attack;


// #endregion

function preload() 
{
 
  game.load.spritesheet('player','assets/sprites/player_spritesheet.png', 48,48);
  game.load.spritesheet('slime','assets/sprites/slime_spritesheet.png', 48,48);
  game.load.spritesheet('top_torch','assets/sprites/torch_top.png',16,16);
  game.load.spritesheet('bot_torch','assets/sprites/torch_bottom.png',16,16);
  game.load.spritesheet('door_vertical','assets/sprites/door_wood_vertical.png',16,16);
  game.load.spritesheet('spikes','assets/sprites/spikes.png',16,16);
  game.load.spritesheet('switch', 'assets/sprites/switch.png',16,16);
  game.load.image("tileset", "assets/maps/tileset.png");

  game.load.tilemap(
    "map",
    "assets/maps/map.json",
    null,
    Phaser.Tilemap.TILED_JSON
  );

  game.load.audio('background','assets/sounds/background_theme.mp3');
  game.load.audio('attack','assets/sounds/hit.wav');
  this.game.scale.scaleMode = Phaser.ScaleManager.USER_SCALE;
  // this.game.scale.setUserScale(5, 5); //this is the game scale
  this.game.scale.setUserScale(1, 1); // this is for debugging
  this.game.renderer.renderSession.roundPixels = true;
  Phaser.Canvas.setImageRenderingCrisp(this.game.canvas);
}

function create() 
{
  // Create map
  map = game.add.tilemap('map', 16, 16, 16, 16);
  map.addTilesetImage('tileset');
  game.world.setBounds(0,0,1600,1600);

  // Use layers
  floor = map.createLayer('Floor');
  floor = map.createLayer('OnFloor'); 

  
  setDoors();
  setButtons();

  music = game.add.audio('background');
  music.loop = true;
  attack = game.add.audio('attack');
  attack.allowMultiple = false;
  // music.play();

  // create player
  player = game.add.sprite(32,32,'player');

  //create slime
  enemies = game.add.group();
  enemies2 = game.add.group();

  // all physics related stuff
  game.physics.startSystem(Phaser.Physics.ARCADE);
  game.physics.enable(player, Phaser.Physics.ARCADE);
  player.body.setSize(16,16,16,16);
  player.body.collideWorldBounds = true;

  // anchor
  player.anchor.set(0,0);

  walls = map.createLayer('Walls');
  floor = map.createLayer('OnWall');
  obsts = map.createLayer('Obstacles');

  // map.setCollisionBetween(1,390);
  map.setCollisionBetween(1, 390, true, 'Obstacles');
  map.setCollisionBetween(1, 390, true, 'Walls');
  above = map.createLayer('AbovePlayer');

  // #region player animations
  // player walk animations
  player.animations.add('idle',[0,1,2],10,true);
  player.animations.add('walk_right', [12,13,14,15],10,true);
  player.animations.add('walk_left', [23,22,21,20],10,true);
  player.animations.add('walk_down', [8,9,10,11],10,true);
  player.animations.add('walk_up', [16,17,18,19],10,true);

  //player attack animations
  player.animations.add('attack_down', [24,25,26,27],10,true);
  player.animations.add('attack_right', [28,29,30,31],10,true);
  player.animations.add('attack_left', [35,34,33,32],10,true);
  player.animations.add('attack_up', [36,37,38,39],10,true);
  // #endregion

  // #region slime stuff

  // slime = game.add.sprite(32,224,'slime');
  // slime2 = game.add.sprite(112,224,'slime');  
  enemies.create(32,224,'slime');
  enemies.create(48,320,'slime');
  enemies.create(832,288,'slime');
  enemies.create(1168,288,'slime');
  enemies.create(832,328,'slime');
  enemies.create(1168,344,'slime');
  enemies2.create(112,224,'slime');
  enemies2.create(144,320,'slime');

  enemies.forEach(function(slime)
  {
    // slime animations
    slime.animations.add('idle', [0,1,2], 10,true);
    slime.animations.add('walk_right',[12,13,14,15],10,true);
    slime.animations.add('walk_left',[16,17,18,19],10,true);
    slime.animations.add('idle', [0,1,2], 10,true);
    slime.animations.add('walk_up',[20,21,22,23],10,true);
    slime.animations.add('walk_down',[11,10,9,8],10,true);
    slime.animations.play('idle');
    
    //set the anchor
    slime.anchor.set(0.5);
    // set physic settings
    game.physics.enable(enemies,Phaser.Physics.ARCADE);
    slime.body.collideWorldBounds = true;
    slime.body.velocity.x = 0;
    slime.body.velocity.y = 0;
    slime.body.velocity.x = slimeSpeed;
    slime.animations.play('walk_right');

    slime.body.setSize(16,16,16,16);
  })
  enemies2.forEach(function(slime)
  {
    // slime animations
    slime.animations.add('idle', [0,1,2], 10,true);
    slime.animations.add('walk_right',[12,13,14,15],10,true);
    slime.animations.add('walk_left',[16,17,18,19],10,true);
    slime.animations.add('idle', [0,1,2], 10,true);
    slime.animations.add('walk_up',[20,21,22,23],10,true);
    slime.animations.add('walk_down',[11,10,9,8],10,true);
    slime.animations.play('idle');
    
    //set the anchor
    slime.anchor.set(0.5,0.5);
    // set physic settings
    game.physics.enable(enemies2,Phaser.Physics.ARCADE);
    slime.body.collideWorldBounds = true;
    slime.body.velocity.x = 0;
    slime.body.velocity.y = 0;
    slime.body.velocity.y = slimeSpeed;
    slime.animations.play('walk_down');

    slime.body.setSize(16,16,16,16);
  })
  
  // #endregion

  // #region start animations
  player.animations.play('idle');
  // #endregion

  // #region set velocities
  player.body.velocity.x = 0;
  player.body.velocity.y = 0;
  // #endregion

  // #region timer
  timer = game.time.create(false);
  timer.loop(5000,updateCounter, this);
  // #endregion

  // #region camera
  game.camera.width = 100;
  game.camera.height = 50;
  game.camera.setSize(100,50);  
  // game.camera.follow(player,Phaser.Camera.FOLLOW_TOPDOWN_TIGHT,0.1,0.1);
  // #endregion

  setTorches();
}

function update() 
{
  // #region collisions
  game.physics.arcade.collide(player,obsts);
  game.physics.arcade.collide(player,walls);
  game.physics.arcade.collide(player,doors);

  game.physics.arcade.collide(player,enemies,playerHit);
  game.physics.arcade.collide(enemies,obsts,changeDirection);
  game.physics.arcade.collide(enemies,walls,changeDirection);
  game.physics.arcade.collide(enemies,doors,changeDirection);
  game.physics.arcade.collide(player,enemies2,playerHit);
  game.physics.arcade.collide(enemies2,obsts,changeDirection);
  game.physics.arcade.collide(enemies2,walls,changeDirection);
  game.physics.arcade.collide(enemies2,doors,changeDirection);

  game.physics.arcade.overlap(player,button1,openDoor);
  game.physics.arcade.overlap(player,button2,openDoor);
  game.physics.arcade.overlap(player,button3,openDoor);
  game.physics.arcade.overlap(player,button4,openDoor);
  // #endregion
  
  player.body.velocity.set(0);  
  
  // #region Walk mechanic
  if(game.input.keyboard.isDown(Phaser.Keyboard.A))
  {
    player.animations.play('walk_left');
    player.body.velocity.x = -100;
  }
  else if (game.input.keyboard.isDown(Phaser.Keyboard.D))
  {
    player.animations.play('walk_right');
    player.body.velocity.x = 100;
  }
  if(game.input.keyboard.isDown(Phaser.Keyboard.W))
  {
    player.animations.play('walk_up');
    player.body.velocity.y = -100;
  }
  else if (game.input.keyboard.isDown(Phaser.Keyboard.S))
  {
    player.animations.play('walk_down');
    player.body.velocity.y = 100;
  } 
  // #endregion

  // #region attack mechanic
  if (game.input.keyboard.isDown(Phaser.Keyboard.LEFT))
  {
    player.animations.play('attack_left');
    isAttacking = true;
    attack.play();
  } 
  if (game.input.keyboard.isDown(Phaser.Keyboard.RIGHT))
  {
    player.animations.play('attack_right');
    isAttacking = true;
    attack.play();
  }
  if (game.input.keyboard.isDown(Phaser.Keyboard.UP))
  {
    player.animations.play('attack_up');
    isAttacking = true;
    attack.play();
  } 
  if (game.input.keyboard.isDown(Phaser.Keyboard.DOWN))
  {
    player.animations.play('attack_down');
    isAttacking = true;
    attack.play(false);
  }
  // #endregion
  
  // #region jump back to idle when not walking or attacking
  else if (
    !game.input.keyboard.isDown(Phaser.Keyboard.A) &&   !game.input.keyboard.isDown(Phaser.Keyboard.D) &&   !game.input.keyboard.isDown(Phaser.Keyboard.W) &&   !game.input.keyboard.isDown(Phaser.Keyboard.S) &&
    !game.input.keyboard.isDown(Phaser.Keyboard.UP) &&   !game.input.keyboard.isDown(Phaser.Keyboard.DOWN) &&   !game.input.keyboard.isDown(Phaser.Keyboard.LEFT) &&   !game.input.keyboard.isDown(Phaser.Keyboard.RIGHT)){
    player.animations.play('idle');
    isAttacking = false;
  }
  //#endregion

}

function updateCounter()
{
  total++;
}

function changeDirection(slime)
{
  // console.log(this.enemies);
  if(slime.body.touching.right || slime.body.blocked.right)
  {
    slime.body.velocity.x = (slimeSpeed*(-1));
    slime.animations.play('walk_left');
  }else if (slime.body.touching.left || slime.body.blocked.left)
  {
    slime.body.velocity.x = (slimeSpeed*(1));
    slime.animations.play('walk_right'); 
  } else if(slime.body.touching.up || slime.body.blocked.up)
  {
    slime.body.velocity.y = (slimeSpeed*(1));
    slime.animations.play('walk_down');
  }else if (slime.body.touching.down || slime.body.blocked.down)
  {
    slime.body.velocity.y = (slimeSpeed*(-1));
    slime.animations.play('walk_up'); 
  }

}

function playerHit(player,slime)
{
  if(isAttacking == true)
  {
    console.log("Player got hit while attacking");
    timer.start();
    slime.kill();
  }else if (isAttacking == false)
  {
  lives -= 1;
  console.log( lives + " left")
  timer.start();
  if(slime.body.velocity.x > 0 && slime.body.velocity.y == 0)
  {
    slime.body.velocity.x = (slimeSpeed*(-1));
    slime.body.velocity.y = 0;
    slime.animations.play('walk_left');
  }else if(slime.body.velocity.x < 0 && slime.body.velocity.y == 0)
  {
    slime.body.velocity.x = (slimeSpeed*(1));
    slime.body.velocity.y = 0;
    slime.animations.play('walk_right');
  }
  }
}

function setTorches()
{
  var torches = game.add.group();

  torch1  = torches.create(48,0,'top_torch');
  torch2  = torches.create(128,96,'top_torch');
  torch3  = torches.create(16,192,'top_torch');
  torch5  = torches.create(112,192,'top_torch');
  torch6  = torches.create(208,192,'top_torch');
  torch7  = torches.create(128,128,'bot_torch');
  torch8  = torches.create(16,336,'bot_torch');
  torch10 = torches.create(112,336,'bot_torch');
  torch12 = torches.create(208,336,'bot_torch');
  torch13 = torches.create(304,224,'top_torch');
  //maze
  torch14 = torches.create(400,0,'top_torch');
  torch15 = torches.create(448,192,'top_torch');
  torch16 = torches.create(480,128,'top_torch');
  torch17 = torches.create(672,192,'top_torch');
  torch18 = torches.create(720,0,'top_torch');
  torch19 = torches.create(448,272,'bot_torch');
  torch20 = torches.create(592,208,'bot_torch');
  torch21 = torches.create(592,304,'bot_torch');
  torch22 = torches.create(688,48,'bot_torch');
  torch23 = torches.create(576,80,'bot_torch');
  torch24 = torches.create(720,304,'bot_torch');
  torch25 = torches.create(432,64,'top_torch');
  torch25 = torches.create(432,64,'top_torch');
  

  torches.forEach(function(torch)
  {
    torch.animations.add('burn1', [0,1,2,3],10,true);
    torch.animations.add('burn2', [1,2,3,0],10,true);
    torch.animations.add('burn3', [2,3,0,1],10,true);
    torch.animations.add('burn4', [3,0,1,2],10,true);
  })
  // start animation
  torch1.animations.play('burn1');
  torch2.animations.play('burn3');
  torch3.animations.play('burn2');
  torch5.animations.play('burn1');
  torch6.animations.play('burn2');
  torch7.animations.play('burn3');
  torch8.animations.play('burn4');
  torch10.animations.play('burn2'); 
  torch12.animations.play('burn4'); 
  torch13.animations.play('burn1'); 
  torch14.animations.play('burn2'); 
  torch15.animations.play('burn3'); 
  torch16.animations.play('burn4'); 
  torch17.animations.play('burn1'); 
  torch18.animations.play('burn2'); 
  torch19.animations.play('burn3'); 
  torch20.animations.play('burn4'); 
  torch21.animations.play('burn1'); 
  torch22.animations.play('burn2'); 
  torch23.animations.play('burn3'); 
  torch24.animations.play('burn4'); 
  torch25.animations.play('burn1'); 
  
  // #endregion
}

function setDoors()
{
  var doors = game.add.group();

  door1  = doors.create(240,240,'door_vertical');
  door1  = doors.create(240,256,'door_vertical');
  door1  = doors.create(736,304,'door_vertical');
  door1  = doors.create(752,304,'door_vertical');
  door1  = doors.create(768,304,'door_vertical');

  doors.forEach(function(door)
  {
    game.physics.enable(door, Phaser.Physics.ARCADE);
    door.body.setSize(16,16,0,0);
    door.anchor.set(0);
    door.body.immovable = true; 
  });

  // door1 = game.add.sprite(240,240,'door_vertical');
  // game.physics.enable(door1, Phaser.Physics.ARCADE);
  // door1.body.setSize(16,16,0,0);
  // door1.anchor.set(0);
  // door1.body.immovable = true; 

  // door5 = game.add.sprite(240,256,'door_vertical');
  // game.physics.enable(door5, Phaser.Physics.ARCADE);
  // door5.body.setSize(16,16,0,0);
  // door5.anchor.set(0);
  // door5.body.immovable = true; 

  // door2 = game.add.sprite(736,304,'door_vertical');
  // game.physics.enable(door2, Phaser.Physics.ARCADE);
  // door2.body.setSize(16,16,0,0);
  // door2.anchor.set(0);
  // door2.body.immovable = true; 

  // door3 = game.add.sprite(752,304,'door_vertical');
  // game.physics.enable(door3, Phaser.Physics.ARCADE);
  // door3.body.setSize(16,16,0,0);
  // door3.anchor.set(0);
  // door3.body.immovable = true; 

  // door4 = game.add.sprite(768,304,'door_vertical');
  // game.physics.enable(door4, Phaser.Physics.ARCADE);
  // door4.body.setSize(16,16,0,0);
  // door4.anchor.set(0);
  // door4.body.immovable = true; 
}

function setButtons()
{
  button1 = game.add.sprite(224,208,'switch');
  button1.animations.add('setLeft',[0],10,true);
  button1.animations.add('setRight',[2],10,true);
  button1.animations.play('setLeft');
  game.physics.enable(button1, Phaser.Physics.ARCADE);
  button1.body.setSize(16,16,0,0);
  button1.anchor.set(0);
  button1.body.immovable = true; 

  button2 = game.add.sprite(528,272,'switch');
  button2.animations.add('setLeft',[0],10,true);
  button2.animations.add('setRight',[2],10,true);
  button2.animations.play('setLeft');
  game.physics.enable(button2, Phaser.Physics.ARCADE);
  button2.body.setSize(16,16,0,0);
  button2.anchor.set(0);
  button2.body.immovable = true; 

  button3 = game.add.sprite(688,80,'switch');
  button3.animations.add('setLeft',[0],10,true);
  button3.animations.add('setRight',[2],10,true);
  button3.animations.play('setLeft');
  game.physics.enable(button3, Phaser.Physics.ARCADE);
  button3.body.setSize(16,16,0,0);
  button3.anchor.set(0);
  button3.body.immovable = true; 

  button4 = game.add.sprite(688,144,'switch');
  button4.animations.add('setLeft',[0],10,true);
  button4.animations.add('setRight',[2],10,true);
  button4.animations.play('setLeft');
  game.physics.enable(button4, Phaser.Physics.ARCADE);
  button4.body.setSize(16,16,0,0);
  button4.anchor.set(0);
  button4.body.immovable = true; 

}

function openDoor(player,button)
{
  
  if(game.input.keyboard.isDown(Phaser.Keyboard.E) && button == button1 && button1Activated == false)
  {
    button.animations.play('setRight');
    door1.visible = false;
    button1Activated = true;
  } 

  if(game.input.keyboard.isDown(Phaser.Keyboard.E) && button == button2 && button2Activated == false)
  {
    button.animations.play('setRight');
    door2.visible = false;
    button2Activated = true;
  } 

  if(game.input.keyboard.isDown(Phaser.Keyboard.E) && button == button3 && button3Activated == false)
  {
    button.animations.play('setRight');
    door3.visible = false;
    button3Activated = true;
  } 

  if(game.input.keyboard.isDown(Phaser.Keyboard.E) && button == button4 && button4Activated == false)
  {
    button.animations.play('setRight');
    door4.visible = false;
    button4Activated = true;
  } 
}
